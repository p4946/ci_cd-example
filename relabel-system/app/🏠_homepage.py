import streamlit as st

from src.general import page_setup


def page_init() -> None:
    page_setup()


def sidebar() -> None:
    # Sidebar
    st.sidebar.success("Select a demo above.")


def body() -> None:
    st.markdown("""
                # Welcome to Demo Page of Relabel System ~
                
                ## About Relabel System and Usage for Wiwynn ~
                
                This system is a tool for relabeling the data of AOI-AI System.

                This system works with the following steps:
                1. Link the data of AOI-AI System and Images of AOI-AI System.
                2. Show out labels from AOI and PSE on images.
                3. If need, PSE is able to relabel the labels on this system.
                4. Output relabeled data to AOI-AI System.
                
                This system is setup by Streamlit API of Python.
                
                ## What we want to show in this quick demo ?
                
                - What we hope AOI system can show to AI.
                - What AI teams cares about the data.
                - To know what PSE cares about the data.
                
                ## Structure of Relabel System
                """)
    system_structure = """app
├── homepage.py
├── pages
│   ├── 1_xml_version.py
│   ├── 2_csv_version.py
│   └── others ..
├── src
│   └── page_inform.py
└── styles
    └── style.css
    """
    st.code(system_structure)
    st.markdown("""
                ## Usage of this system
                
                This system match data information and images. And setup two ways to get image information.
                
                1. XML files
                2. CSV files
                
                For both version, system structure must match the following structure to make sure system is able to get images.
                
                ### Structures of system and datas
                
                The structure of datas follows AI Center XML/IMG data structure.
                
                CSV files can be upload while need.
                """)
    sys_data_structures = """root directory (relabel-system)
├── app <- system code structure can look at structure above
└── data
    ├── IMG
    │   ├── 2022-02-24
    │   │   ├── S3B_POSTAOI
    │   │   │   ├── 19W02-1-02601M037-T01
    │   │   │   │   ├── 20220224224614_WN8320800HN01
    │   │   │   │   │   └── images
    │   │   │   │   └── other board sns ..
    │   │   │   └── other cmodels ..
    │   │   └── other stations ..
    │   ├── 2022-02-25
    │   └── other date ..
    └── XML
        ├── 2022-02-24
        │   ├── S3B_POSTAOI
        │   │   ├── 19W02-1-02601M037-T01
        │   │   │   ├── WN8320800HN01
        │   │   │   │   └── xml files
        │   │   │   └── other board sns ..
        │   │   └── other cmodels ..
        │   └── other stations ..
        ├── 2022-02-25
        └── other date ..
    """
    st.code(sys_data_structures)


def main() -> None:
    page_init()
    sidebar()
    body()


if __name__ == '__main__':
    main()
