import os
from pathlib import Path

import cv2
import numpy as np
import pandas as pd
import streamlit as st
from lxml import etree

from src.general import page_setup, local_css
from src.sidebar_button import prev_sn_index, next_sn_index, reset_sn_index, update_sn_index
from src.relabel import output_dataframe_prepare, reset_relabeled_data


def session_init() -> None:
    
    if "sn_index" not in st.session_state:
        st.session_state.sn_index = 0
    if "sn_list" not in st.session_state:
        st.session_state.sn_list = []
    if "xml_paths" not in st.session_state:
        st.session_state.xml_paths = []
    if "sidebar_errors" not in st.session_state:
        st.session_state.sidebar_errors = []
    if "xml_path_errors" not in st.session_state:
        st.session_state.xml_path_errors = []
    if "img_show_errors" not in st.session_state:
        st.session_state.img_show_errors = []
    if "relabel_dict" not in st.session_state:
        st.session_state.relabel_dict = {}


def page_init() -> None:
    
    page_setup()
    local_css(Path("./styles/style.css"))
    session_init()


def sidebar_layout() -> None:
    
    st.session_state.sidebar_xmlselect = st.sidebar.container()
    st.session_state.sidebar_tools = st.sidebar.container()
    st.session_state.sidebar_message = st.sidebar.container()


def sidebar_selectbox(label: str, options: list, state_name: str) -> None:
    
    if state_name == "sn":
        return st.session_state.sidebar_sn_right.selectbox(label=label,
                                                           options=options,
                                                           index=st.session_state[f"{state_name}_index"],
                                                           key=f"{state_name}_selectbox",
                                                           on_change=update_sn_index)
    return st.session_state.sidebar_xmlselect.selectbox(label=label,
                                                        options=options,
                                                        key=f"{state_name}_selectbox",
                                                        on_change=reset_sn_index)


def decimal_to_any(number: int) -> str:
    """
    Converter of decimal to any.

    Args:
        number: original number.
    Returns: 
        result: the number after converting.
    """
    loop = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    result = ''
    while number != 0:
        result = loop[number % len(loop)] + result
        number //= len(loop)
    
    return result


def board_sn_fix(xml_path: Path, sn_option: str) -> str:
    for i in range(1, 5):
        tmp_sn_option = sn_option[:-3]
        tmp_sn_option = decimal_to_any(int(tmp_sn_option, 36)-i) + sn_option[-3:]
        if os.path.exists(xml_path / tmp_sn_option):
            
            return tmp_sn_option
        
        tmp_sn_option = sn_option[:-3]
        tmp_sn_option = decimal_to_any(int(tmp_sn_option, 36)+i) + sn_option[-3:]
        if os.path.exists(xml_path / tmp_sn_option):
            
            return tmp_sn_option
    
    st.session_state.xml_path_errors.append(f"No SN {sn_option} in XML")
    
    return ""


def sn_check(xml_path: Path, sn_option: str) -> Path:
    
    return xml_path / sn_option if os.path.exists(xml_path / sn_option) else xml_path / board_sn_fix(xml_path, sn_option)


def sn_select(xml_path: Path):
    
    st.session_state.sn_list = os.listdir(xml_path)
    st.session_state.sn_list.sort()
    st.session_state.sidebar_sn_left, st.session_state.sidebar_sn_right = \
        st.session_state.sidebar_xmlselect.columns([2, 3])
    
    sn_method = st.session_state.sidebar_sn_left.radio("How to find SN",
                                                       options=["Select", "Search"])
    if sn_method == "Select":
        sn_prev, sn_next = st.session_state.sidebar_xmlselect.columns(2)
        if sn_prev.button("Previous SN", on_click=prev_sn_index):
            sn_option = st.session_state.sn_list[st.session_state["sn_index"]]
        if sn_next.button("Next SN", on_click=next_sn_index):
            sn_option = st.session_state.sn_list[st.session_state["sn_index"]]
        sn_option = sidebar_selectbox("Select SN", st.session_state.sn_list, "sn")
        
        return xml_path / sn_option, sn_option
    
    sn_option = st.session_state.sidebar_sn_right.text_input("Search SN", value=f"{st.session_state.sn_list[0]}")
    
    return sn_check(xml_path, sn_option), sn_option


def select_xml_paths(xml_root: Path) -> list:
    
    xml_path = xml_root
    
    # date select
    times = os.listdir(xml_path)
    times.sort()
    date_option = sidebar_selectbox("Select Date", times, "date")
    xml_path = xml_path / date_option
    
    # stations select
    stations = os.listdir(xml_path)
    stations.sort()
    station_option = sidebar_selectbox("Select Station", stations, "station")
    xml_path = xml_path / station_option
    
    # cmodels select
    cmodels = os.listdir(xml_path)
    cmodels.sort()
    cmodel_option = sidebar_selectbox("Select CModel", cmodels, "cmodel")
    xml_path = xml_path / cmodel_option
    
    # sn select
    xml_path, sn_option = sn_select(xml_path)
    if st.session_state.xml_path_errors:
        
        return []
    
    # xml select
    xmls = os.listdir(xml_path)
    
    return [str(xml_path / xml) for xml in xmls if sn_option in xml]


def sidebar(xml_root: Path) -> None:
    
    sidebar_layout()
    st.session_state.xml_paths = select_xml_paths(xml_root)
    st.session_state.img_show, st.session_state.download_button = st.session_state.sidebar_tools.columns(2)
    st.session_state.image_show = st.session_state.img_show.radio("How to show images",
                                                                  options=("origin", "crop_half", "crop_component"))
    
    if st.session_state.sidebar_errors:
        for error in st.session_state.sidebar_errors:
            st.session_state.sidebar_message.warning(error)
        st.session_state.sidebar_errors = []
    if st.session_state.xml_paths:
        for xml_path in st.session_state.xml_paths:
            st.session_state.sidebar_message.info(f"XML file name: {xml_path.split('/')[-1]}")


def image_root_detect(xml_path: str) -> Path:

    paths = xml_path.split(sep='/')
    image_root = f"{paths[0]}/{paths[1]}/{paths[2]}/{paths[3]}/{paths[4]}/{paths[5]}/{paths[7][:-4]}"
    image_root = Path(image_root.replace("XML", "IMG"))
    
    return image_root


def image_inform_detect(xml_path: str) -> dict:
    
    img_informs = {}
    xml_file = etree.parse(xml_path)
    board_sn = xml_path.split(sep='/')[-1].split(sep='_')[-1][:-4]
    root = xml_file.getroot()
    for board in root.iter("Board"):
        if board.get("BoardSN") != board_sn:
            continue
        for component in board.iter("Component"):
            img_inform = {}
            for component_img in component.iter("CompImage"):
                for image in component_img.iter("Image"):
                    img_inform["pic_name"] = image.get("PicPath").split("\\")[-1]
                    img_inform["x1"], img_inform["y1"], img_inform["x2"], img_inform["y2"] = \
                        image.get("X1"), image.get("Y1"), image.get("X2"), image.get("Y2")
            windows = [{"board_sn": board_sn,
                        "image_name": img_inform["pic_name"],
                        "win_name": window.get("Name"),
                        "status": window.get("Status"),
                        "pse_ans": "NG" if window.get("Status") == "REPAIR" else "OK",
                        "machine_defect": window.get("MachineDefect"), 
                        "x1": window.get("X1"), 
                        "y1": window.get("Y1"), 
                        "x2": window.get("X2"), 
                        "y2": window.get("Y2"), 
                        "angle": window.get("Angle"),
                        "PinNum": window.get("PinNum"),
                        "Algorithm": window.get("Algorithm")} \
                            for window in component.iter("Window")]

            img_inform["windows"] = windows
            img_informs[img_inform['pic_name']] = img_inform

    return img_informs


def __img_window_detect(img: np.ndarray, windows: list):
    
    img_docs = []
    img_drow = img.copy()
    for window in windows:
        win_name = window["win_name"]
        win_status = window["status"]
        win_x1, win_y1, win_x2, win_y2, win_angle = \
            int(window["x1"]), int(window["y1"]), int(window["x2"]), int(window["y2"]), int(window["angle"])
        if win_angle in {90, 270}:
            centerx, centery = (win_x1+win_x2)/2, (win_y1+win_y2)/2
            w, h = win_x2-win_x1, win_y2-win_y1
            win_x1, win_x2 = int(centerx-h/2), int(centerx+h/2)
            win_y1, win_y2 = int(centery-w/2), int(centery+w/2)
        # draw window
        img_drow = cv2.rectangle(img_drow, (win_x1, win_y1), (win_x2, win_y2), (0, 255, 0), 2)
        cv2.putText(img_drow, f"{win_name.split('~')[-1]}_AOI", (win_x1, win_y1-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
        if win_status == "REPAIR":
            img_drow = cv2.rectangle(img_drow, (win_x1, win_y1), (win_x2, win_y2), (255, 0, 0), 2)
            cv2.putText(img_drow, f"{win_name.split('~')[-1]}_PSE", (win_x1, win_y2+25), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (255, 0, 0), 2)
        img_docs.append({"image name": window["image_name"],
                         "board sn": window["board_sn"],
                         "window name": win_name,
                         "window status": win_status,
                         "machine defect": window["machine_defect"],
                         "PSE answer": window["pse_ans"],
                         "PinNum": window["PinNum"],
                         "Algorithm": window["Algorithm"]})
    return img_drow, img_docs


def button_handle(img_docs: list, index: int, button: st.container):
    button_key = \
        f"{img_docs[index]['image name'][:-4]}_{img_docs[index]['board sn']}_{img_docs[index]['window name']}_{img_docs[index]['window status']}"
    button_hold = button.empty()
    button_press = button_hold.button("Unlabel" if button_key in st.session_state.relabel_dict else "Relabel",
                                      key=button_key)
    if button_press:
        if button_key in st.session_state.relabel_dict:
            del st.session_state.relabel_dict[button_key]
        else:
            save_img_docs = img_docs[index]
            if save_img_docs["PSE answer"] == "OK":
                save_img_docs["relabel"] = "NG"
            else:
                save_img_docs["relabel"] = "OK"
            st.session_state.relabel_dict[button_key] = save_img_docs
        button_hold.empty()
        button_hold.button("Unlabel" if button_key in st.session_state.relabel_dict else "Relabel",
                           key=button_key)


def image_data_show(image_root: Path, image_informs: dict) -> None:
    image_names = os.listdir(image_root)
    for image_name in image_names:
        try:
            img_inform = image_informs[image_name]
            img = cv2.imread(str(image_root/image_name), cv2.IMREAD_COLOR)
            windows = img_inform["windows"]
            img_draw, img_docs = __img_window_detect(img, windows)
            
            if st.session_state.image_show == "crop_half":
                hei, wei, _ = img.shape
                img = img[int(img_inform["y1"])//2:int((hei-int(img_inform["y2"]))/2)+int(img_inform["y2"]), 
                            int(img_inform["x1"])//2:int((wei-int(img_inform["x2"]))/2)+int(img_inform["x2"])]
                img_draw = img_draw[int(img_inform["y1"])//2:int((hei-int(img_inform["y2"]))/2)+int(img_inform["y2"]), 
                                    int(img_inform["x1"])//2:int((wei-int(img_inform["x2"]))/2)+int(img_inform["x2"])]
            elif st.session_state.image_show == "crop_component":
                img = img[int(img_inform["y1"]):int(img_inform["y2"]), int(img_inform["x1"]):int(img_inform["x2"])]
                img_draw = \
                    img_draw[int(img_inform["y1"]):int(img_inform["y2"]), int(img_inform["x1"]):int(img_inform["x2"])]
            
            img_org, img_rect = st.columns(2)
            img_org.image(img, caption=f"{img_inform['pic_name']}_origin", use_column_width='auto')
            img_rect.image(img_draw, caption=f"{img_inform['pic_name']}_windows", use_column_width='auto')
            colms = st.columns([2, 2, 2, 1.5, 1.2, 1.5, 1])
            fields = ["Window Name", "Window Status", "Machine Defect", "PSE Answer", "PinNum", "Algorithm", "Relabel"]
            for col, field_name in zip(colms, fields):
                # header
                col.write(field_name)
            window_name, window_status, machine_defect, pse_ans, pin_num, algo, relabel_button = \
                st.columns([2, 2, 2, 1.5, 1.2, 1.5, 1])
            for i in range(len(img_docs)):
                window_name.write(img_docs[i]["window name"])
                window_status.write(img_docs[i]["window status"])
                machine_defect.write(img_docs[i]["machine defect"])
                pse_ans.write(img_docs[i]["PSE answer"])
                pin_num.write(img_docs[i]["PinNum"])
                algo.write(img_docs[i]["Algorithm"])
                
                button_handle(img_docs, i, relabel_button)
                
        except Exception:
            st.session_state.img_show_errors.append(f"**No Image infromation of {image_name} in XML**")


def body() -> None:
    
    if not st.session_state.xml_path_errors and st.session_state.xml_paths:
        for xml_path in st.session_state.xml_paths:
            
            image_root = image_root_detect(xml_path)
            image_informs = image_inform_detect(xml_path)
            image_data_show(image_root, image_informs)
    
    if st.session_state.relabel_dict:
        csv = output_dataframe_prepare()
        st.session_state.download_button.download_button("Download Relabeled Data",
                                                         data=csv,
                                                         file_name="relabeled_data.csv",
                                                         key="download_csv",
                                                         on_click=reset_relabeled_data)
    
    if st.session_state.xml_path_errors:
        for error_msg in st.session_state.xml_path_errors:
            st.error(error_msg)
        st.session_state.xml_path_errors = []
    if st.session_state.img_show_errors:
        for error_msg in st.session_state.img_show_errors:
            st.error(error_msg)
        st.session_state.img_show_errors = []
    if not st.session_state.xml_paths:
        st.error("No xml files, something went wrong")


def main() -> None:
    
    page_init()
    sidebar(Path("../data/XML"))
    body()


if __name__ == "__main__":
    
    main()
