import os
from ast import literal_eval
from pathlib import Path

import cv2
import numpy as np
import pandas as pd
import streamlit as st

from src.general import page_setup, local_css
from src.sidebar_button import prev_sn_index, next_sn_index, reset_sn_index, update_sn_index
from src.relabel import output_dataframe_prepare, reset_relabeled_data


def session_init() -> None:
    if "csv_uploaded" not in st.session_state:
        st.session_state.csv_uploaded = False
    if "csv_filename" not in st.session_state:
        st.session_state.csv_filename = ""
    if "csv_dataframe" not in st.session_state:
        st.session_state.csv_dataframe = pd.DataFrame()
    if "sn_index" not in st.session_state:
        st.session_state.sn_index = 0
    if "sn_list" not in st.session_state:
        st.session_state.sn_list = []
    if "img_root" not in st.session_state:
        st.session_state.img_root = ""
    if "sidebar_errors" not in st.session_state:
        st.session_state.sidebar_errors = []
    if "img_path_errors" not in st.session_state:
        st.session_state.img_path_errors = []
    if "img_show_errors" not in st.session_state:
        st.session_state.img_show_errors = []
    if "relabel_dict" not in st.session_state:
        st.session_state.relabel_dict = {}


def page_init() -> None:
    
    page_setup()
    local_css(Path("./styles/style.css"))
    session_init()


def sidebar_layout() -> None:
    
    st.session_state.sidebar_upload = st.sidebar.container()
    st.session_state.sidebar_dataframeselect = st.sidebar.container()
    st.session_state.sidebar_tools = st.sidebar.container()
    st.session_state.sidebar_message = st.sidebar.container()


def reset_dataframe() -> None:
    
    st.session_state.csv_dataframe = pd.DataFrame()
    st.session_state.csv_uploaded = False
    st.session_state.csv_filename = ""


def sidebar_upload() -> None:
    
    uploaded_file = st.session_state.sidebar_upload.file_uploader("Update CSV file",
                                                                  key="csv_file_upload",
                                                                  on_change=reset_dataframe)
    if uploaded_file is not None:
        st.session_state.csv_filename = uploaded_file.name
        st.session_state.csv_uploaded = True
        st.session_state.csv_dataframe = pd.read_csv(uploaded_file, sep=';')


def sidebar_selectbox(label: str, options: list, state_name: str) -> None:
    
    if state_name == "sn":
        return st.session_state.sidebar_sn_right.selectbox(label=label,
                                                           options=options,
                                                           index=st.session_state[f"{state_name}_index"],
                                                           key=f"{state_name}_selectbox",
                                                           on_change=update_sn_index)
    return st.session_state.sidebar_dataframeselect.selectbox(label=label,
                                                              options=options,
                                                              key=f"{state_name}_selectbox",
                                                              on_change=reset_sn_index)


def sn_select(dataframe: pd.DataFrame):
    
    st.session_state.sn_list = sorted(dataframe.groupby('board_sn').count().index)
    st.session_state.sidebar_sn_left, st.session_state.sidebar_sn_right = \
        st.session_state.sidebar_dataframeselect.columns([2, 3])

    sn_method = st.session_state.sidebar_sn_left.radio("How to find SN",
                                                       options=["Select", "Search"])
    if sn_method == "Select":
        sn_prev, sn_next = st.session_state.sidebar_dataframeselect.columns(2)
        if sn_prev.button("Previous SN", on_click=prev_sn_index):
            sn_option = st.session_state.sn_list[st.session_state["sn_index"]]
        if sn_next.button("Next SN", on_click=next_sn_index):
            sn_option = st.session_state.sn_list[st.session_state["sn_index"]]
        sn_option = sidebar_selectbox("Select SN", st.session_state.sn_list, "sn")
    else:
        sn_option = st.session_state.sidebar_sn_right.text_input("Search SN", value=f"{st.session_state.sn_list[0]}")
    dataframe = dataframe[dataframe['board_sn'] == sn_option].reset_index(drop=True)

    return dataframe


def select_dataframes():
    
    dataframe = st.session_state.csv_dataframe

    # date select
    times = sorted(dataframe.groupby('date').count().index)
    date_option = sidebar_selectbox("Select Date", times, "date")
    dataframe = dataframe[dataframe['date'] == date_option].reset_index(drop=True)

    # stations select
    stations = sorted(dataframe.groupby('stations').count().index)
    station_option = sidebar_selectbox("Select Station", stations, "station")
    dataframe = dataframe[dataframe['stations'] == station_option].reset_index(drop=True)

    # cmodels select
    cmodels = sorted(dataframe.groupby('cmodels').count().index)
    cmodel_option = sidebar_selectbox("Select CModel", cmodels, "cmodel")
    dataframe = dataframe[dataframe['cmodels'] == cmodel_option].reset_index(drop=True)

    # sn select
    st.session_state.csv_dataframe = sn_select(dataframe)
    if st.session_state.csv_dataframe.empty:
        st.session_state.img_path_errors.append("Wrong SN is given. Please try again.")


def sidebar() -> None:
    
    sidebar_layout()
    sidebar_upload()
    if st.session_state.csv_uploaded:
        select_dataframes()
        st.session_state.img_show, st.session_state.download_button = st.session_state.sidebar_tools.columns(2)
        st.session_state.image_show = st.session_state.img_show.radio("How to show images",
                                                                      options=("origin", "crop_half", "crop_component"))
        if st.session_state.sidebar_errors:
            for error in st.session_state.sidebar_errors:
                st.session_state.sidebar_message.warning(error)
            st.session_state.sidebar_errors = []


def image_root_detect(img_root: str) -> str:
    dataframe = st.session_state.csv_dataframe.iloc[0]
    
    return f"{img_root}/{dataframe['date']}/{dataframe['stations']}/{dataframe['cmodels']}/{dataframe['xml_file_name'][:-4]}"


def image_inform_detect():
    
    img_informs = {}
    for i in range(len(st.session_state.csv_dataframe)):
        img_inform = literal_eval(st.session_state.csv_dataframe.iloc[i]['image_inform'])
        windows = literal_eval(st.session_state.csv_dataframe.iloc[i]['windows_inform'])
        for window in windows:
            window["board_sn"] = st.session_state.csv_dataframe.iloc[i]["board_sn"]
            window["image name"] = img_inform["image name"]
            window["pse_answer"] = st.session_state.csv_dataframe.iloc[i]["pse_answer"]
            window["real_pse_answer"] = st.session_state.csv_dataframe.iloc[i]["real_pse_answer"]
        img_inform["windows"] = windows
        img_informs[img_inform["image name"]] = img_inform
    
    return img_informs


def __img_window_detect(img: np.ndarray, windows: list):
    
    img_docs = []
    img_drow = img.copy()
    for window in windows:
        win_name = window["name"]
        win_status = window["status"]
        win_x1, win_y1, win_x2, win_y2, win_angle = \
            int(window["x1"]), int(window["y1"]), int(window["x2"]), int(window["y2"]), int(window["angle"])
        if win_angle in {90, 270}:
            centerx, centery = (win_x1+win_x2)/2, (win_y1+win_y2)/2
            w, h = win_x2-win_x1, win_y2-win_y1
            win_x1, win_x2 = int(centerx-h/2), int(centerx+h/2)
            win_y1, win_y2 = int(centery-w/2), int(centery+w/2)
        # draw window
        img_drow = cv2.rectangle(img_drow, (win_x1, win_y1), (win_x2, win_y2), (0, 255, 0), 2)
        cv2.putText(img_drow, f"{win_name.split('~')[-1]}_AOI", (win_x1, win_y1-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
        if win_status == "REPAIR":
            img_drow = cv2.rectangle(img_drow, (win_x1, win_y1), (win_x2, win_y2), (255, 0, 0), 2)
            cv2.putText(img_drow, f"{win_name.split('~')[-1]}_PSE", (win_x1, win_y2+25), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (255, 0, 0), 2)
        img_docs.append({"image name": window["image name"],
                         "board sn": window["board_sn"],
                         "window name": win_name,
                         "window status": win_status,
                         "machine defect": window["machine_defect"],
                         "pse_answer": window["pse_answer"],
                         "real_pse_answer": window["real_pse_answer"]})
    
    return img_drow, img_docs


def button_handle(img_docs: list, index: int, button: st.container):
    button_key = \
        f"{img_docs[index]['image name'][:-4]}_{img_docs[index]['board sn']}_{img_docs[index]['window name']}_{img_docs[index]['window status']}"
    button_hold = button.empty()
    button_press = button_hold.button("Unlabel" if button_key in st.session_state.relabel_dict else "Relabel",
                                      key=button_key)
    if button_press:
        if button_key in st.session_state.relabel_dict:
            del st.session_state.relabel_dict[button_key]
        else:
            save_img_docs = img_docs[index]
            if save_img_docs["real_pse_answer"] == "OK":
                save_img_docs["relabel"] = "NG"
            else:
                save_img_docs["relabel"] = "OK"
            st.session_state.relabel_dict[button_key] = save_img_docs
        button_hold.empty()
        button_hold.button("Unlabel" if button_key in st.session_state.relabel_dict else "Relabel",
                           key=button_key)


def image_data_show(image_root: Path, image_informs: dict) -> None:
    
    try:
        image_names = os.listdir(image_root)
        for image_name in image_names:
            try:
                img_inform = image_informs[image_name]
                img = cv2.imread(str(image_root/image_name), cv2.IMREAD_COLOR)
                windows = img_inform["windows"]
                img_draw, img_docs = __img_window_detect(img, windows)
                if st.session_state.image_show == "crop_component":
                    img = img[int(img_inform["y1"]):int(img_inform["y2"]), int(img_inform["x1"]):int(img_inform["x2"])]
                    img_draw = \
                        img_draw[int(img_inform["y1"]):int(img_inform["y2"]), int(img_inform["x1"]):int(img_inform["x2"])]

                elif st.session_state.image_show == "crop_half":
                    hei, wei, _ = img.shape
                    img = img[int(img_inform["y1"])//2:int((hei-int(img_inform["y2"]))/2)+int(img_inform["y2"]), 
                                int(img_inform["x1"])//2:int((wei-int(img_inform["x2"]))/2)+int(img_inform["x2"])]
                    img_draw = img_draw[int(img_inform["y1"])//2:int((hei-int(img_inform["y2"]))/2)+int(img_inform["y2"]), 
                                        int(img_inform["x1"])//2:int((wei-int(img_inform["x2"]))/2)+int(img_inform["x2"])]
                img_org, img_rect = st.columns(2)
                img_org.image(img, caption=f"{img_inform['image name']}_origin", use_column_width='auto')
                img_rect.image(img_draw, caption=f"{img_inform['image name']}_windows", use_column_width='auto')
                colms = st.columns([2, 2, 2, 1, 2, 1])
                fields = ["Window Name", "Window Status", "Machine Defect", "PSE Answer", "Real PSE Answer", "Relabel"]
                for col, field_name in zip(colms, fields):
                    # header
                    col.write(field_name)
                window_name, window_status, machine_defect, pse_ans, real_pse_ans, relabel_button = colms = st.columns([2, 2, 2, 1, 1, 1])
                for i in range(len(img_docs)):
                    window_name.write(img_docs[i]["window name"])
                    window_status.write(img_docs[i]["window status"])
                    machine_defect.write(img_docs[i]["machine defect"])
                    pse_ans.write(img_docs[i]["pse_answer"])
                    real_pse_ans.write(img_docs[i]["real_pse_answer"])
                    
                    button_handle(img_docs, i, relabel_button)
            except Exception:
                st.session_state.img_show_errors.append(f"**No Image infromation of {image_name} in XML**")
    except Exception:
        st.session_state.img_show_errors.append("**Image root pass is not able to find**")


def body(img_root: str) -> None:
    
    if not st.session_state.img_path_errors and not st.session_state.csv_dataframe.empty:
        image_root = image_root_detect(img_root)
        image_informs = image_inform_detect()
        image_data_show(Path(image_root), image_informs)
    
    if st.session_state.relabel_dict:
        csv = output_dataframe_prepare()
        st.session_state.download_button.download_button("Download Relabeled Data",
                                                         data=csv,
                                                         file_name="relabeled_data.csv",
                                                         key="download_csv",
                                                         on_click=reset_relabeled_data)
    
    if st.session_state.img_path_errors:
        for error_msg in st.session_state.img_path_errors:
            st.error(error_msg)
        st.session_state.img_path_errors = []
    if st.session_state.img_show_errors:
        for error_msg in st.session_state.img_show_errors:
            st.error(error_msg)
        st.session_state.img_show_errors = []


def main() -> None:
    
    page_init()
    sidebar()
    body("../data/IMG")


if __name__ == '__main__':
    main()
