from pathlib import Path

import streamlit as st


def page_setup() -> None:
    # Page Inform
    st.set_page_config(
        page_title="Relabel System Demo",
        layout="wide",
        initial_sidebar_state="auto",
        menu_items={
            'About': "This application is host by **Wiwynn Data Analysis Team**"
        }
    )

def local_css(path: Path) -> None:
    
    with open(path) as css:
        st.markdown(f'<style>{css.read()}</style>', unsafe_allow_html=True)
