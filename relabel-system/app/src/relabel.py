import pandas as pd
import streamlit as st


def output_dataframe_prepare() -> bytes:
    dataframe = pd.DataFrame(st.session_state.relabel_dict.values()).drop_duplicates()

    return dataframe.to_csv(index=False).encode('utf-8')


def reset_relabeled_data() -> None:
    
    st.session_state.relabel_dict = {}
