import streamlit as st


def prev_sn_index() -> None:
    st.session_state.sn_index -= 1
    if st.session_state.sn_index < 0:
        st.session_state.sn_index = 0
        st.session_state.sidebar_errors.append("This is the first SN")


def next_sn_index() -> None:
    st.session_state.sn_index += 1
    if st.session_state.sn_index >= len(st.session_state.sn_list):
        st.session_state.sn_index = len(st.session_state.sn_list) - 1
        st.session_state.sidebar_errors.append("This is the last SN")


def reset_sn_index() -> None:
    
    st.session_state.sn_index = 0


def update_sn_index() -> None:
    
    st.session_state.sn_index = st.session_state.sn_list.index(st.session_state.sn_selectbox)
